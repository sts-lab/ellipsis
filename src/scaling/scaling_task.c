#define _GNU_SOURCE
#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include <sys/types.h>
#include <pthread.h>
#include <stdatomic.h>

#define NSEC_PER_SEC (1000 * 1000 * 1000ULL)

long long timespec_subtract(struct timespec *a, struct timespec *b)
{
    long long ns;
    ns = (b->tv_sec - a->tv_sec) * NSEC_PER_SEC;
    ns += (b->tv_nsec - a->tv_nsec);
    return ns;
}

int main(int argc, char *argv[])
{
    int i, j;
    uint len, reduce;
    long long duration;
    struct timespec start_data, stop_data;

    assert(argc == 3);
    len = atoi(argv[1]);
    reduce = atoi(argv[2]);

    //Setup sched affinity to the core
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(3, &cpuset); // Set core 3 to run benchmark
    pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);

    clock_gettime(CLOCK_MONOTONIC, &start_data);
    for (i = 0; i < len - 1; i++) {
        getpid();
    }

    if (reduce == 1) {
        getpid();
    } else {
        getppid();
    }
    clock_gettime(CLOCK_MONOTONIC, &stop_data);

    duration = timespec_subtract(&start_data, &stop_data);
    printf("%lld", duration);
    return 0;
}
