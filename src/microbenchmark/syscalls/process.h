#include <time.h>
#include <unistd.h>


void run_getpid(struct timespec* start_data,struct timespec* stop_data,struct timespec* empty_start,struct timespec* empty_stop){
		clock_gettime(CLOCK_MONOTONIC,start_data);
		getpid();
		clock_gettime(CLOCK_MONOTONIC,stop_data);

		clock_gettime(CLOCK_MONOTONIC,empty_start);
		clock_gettime(CLOCK_MONOTONIC,empty_stop);
}

void run_getppid(struct timespec* start_data,struct timespec* stop_data,struct timespec* empty_start,struct timespec* empty_stop){
		clock_gettime(CLOCK_MONOTONIC,start_data);
		getppid();
		clock_gettime(CLOCK_MONOTONIC,stop_data);

		clock_gettime(CLOCK_MONOTONIC,empty_start);
		clock_gettime(CLOCK_MONOTONIC,empty_stop);
}

void run_getgid(struct timespec* start_data,struct timespec* stop_data,struct timespec* empty_start,struct timespec* empty_stop){
		clock_gettime(CLOCK_MONOTONIC,start_data);
		getgid();
		clock_gettime(CLOCK_MONOTONIC,stop_data);

		clock_gettime(CLOCK_MONOTONIC,empty_start);
		clock_gettime(CLOCK_MONOTONIC,empty_stop);
}

void run_getuid(struct timespec* start_data,struct timespec* stop_data,struct timespec* empty_start,struct timespec* empty_stop){
		clock_gettime(CLOCK_MONOTONIC,start_data);
		getuid();
		clock_gettime(CLOCK_MONOTONIC,stop_data);

		clock_gettime(CLOCK_MONOTONIC,empty_start);
		clock_gettime(CLOCK_MONOTONIC,empty_stop);
}

void run_getpgrp(struct timespec* start_data,struct timespec* stop_data,struct timespec* empty_start,struct timespec* empty_stop){
		clock_gettime(CLOCK_MONOTONIC,start_data);
		getpgrp();
		clock_gettime(CLOCK_MONOTONIC,stop_data);

		clock_gettime(CLOCK_MONOTONIC,empty_start);
		clock_gettime(CLOCK_MONOTONIC,empty_stop);
}


void run_geteuid(struct timespec* start_data,struct timespec* stop_data,struct timespec* empty_start,struct timespec* empty_stop){
		clock_gettime(CLOCK_MONOTONIC,start_data);
		geteuid();
		clock_gettime(CLOCK_MONOTONIC,stop_data);

		clock_gettime(CLOCK_MONOTONIC,empty_start);
		clock_gettime(CLOCK_MONOTONIC,empty_stop);
}
