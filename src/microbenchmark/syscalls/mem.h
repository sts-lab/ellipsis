#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <time.h>


//"/tmp" has a in memory filesystem mounted to avoid disk write latencies
void run_mprotect(struct timespec* start_data,struct timespec* stop_data,struct timespec* empty_start,struct timespec* empty_stop){
    int pagesize = sysconf(_SC_PAGE_SIZE);
    void *buffer = memalign(pagesize, 4 * pagesize);
	clock_gettime(CLOCK_MONOTONIC,start_data);
	mprotect(buffer + pagesize * 2, pagesize,
                       PROT_READ);
	clock_gettime(CLOCK_MONOTONIC,stop_data);
	
    clock_gettime(CLOCK_MONOTONIC,empty_start);
	clock_gettime(CLOCK_MONOTONIC,empty_stop);

    free(buffer);

}