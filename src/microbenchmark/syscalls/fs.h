#include<stdio.h>
#include <unistd.h>
#include<time.h>
#include<fcntl.h>
#include<string.h>


//"/tmp" has a in memory filesystem mounted to avoid disk write latencies
void run_write(struct timespec* start_data,struct timespec* stop_data,struct timespec* empty_start,struct timespec* empty_stop){
	char buf[] = "input\n";
	int fd = open("/tmp/temp.txt",O_CREAT|O_WRONLY|O_APPEND);
	clock_gettime(CLOCK_MONOTONIC,start_data);
	write(fd,buf,strlen(buf));
	clock_gettime(CLOCK_MONOTONIC,stop_data);
	close(fd);
	remove("/tmp/temp.txt");

	clock_gettime(CLOCK_MONOTONIC,empty_start);
	clock_gettime(CLOCK_MONOTONIC,empty_stop);

}

void run_read(struct timespec* start_data,struct timespec* stop_data,struct timespec* empty_start,struct timespec* empty_stop){
	char buf[10];
	int fd = open("/tmp/temp_read.txt",O_CREAT|O_RDONLY);
	clock_gettime(CLOCK_MONOTONIC,start_data);
	read(fd,buf,6);
	clock_gettime(CLOCK_MONOTONIC,stop_data);
	close(fd);
	remove("/tmp/temp_read.txt");

	clock_gettime(CLOCK_MONOTONIC,empty_start);
	clock_gettime(CLOCK_MONOTONIC,empty_stop);

}

void run_close(struct timespec* start_data,struct timespec* stop_data,struct timespec* empty_start,struct timespec* empty_stop){
	int fd = open("/tmp/temp_read.txt",O_CREAT|O_RDONLY);
	clock_gettime(CLOCK_MONOTONIC,start_data);
	close(fd);
	clock_gettime(CLOCK_MONOTONIC,stop_data);

	remove("/tmp/temp_read.txt");

	clock_gettime(CLOCK_MONOTONIC,empty_start);
	clock_gettime(CLOCK_MONOTONIC,empty_stop);
}

void run_openat(struct timespec* start_data,struct timespec* stop_data,struct timespec* empty_start,struct timespec* empty_stop){
	int fd = open("/tmp/temp.txt",O_CREAT|O_RDONLY);
	close(fd);

	int temp_dir_fd = open("/tmp/",O_RDONLY);
	clock_gettime(CLOCK_MONOTONIC,start_data);
	int fd1 = openat(temp_dir_fd,"temp.txt",O_RDONLY);
	clock_gettime(CLOCK_MONOTONIC,stop_data);

	clock_gettime(CLOCK_MONOTONIC,empty_start);
	clock_gettime(CLOCK_MONOTONIC,empty_stop);
	close(fd1);
	remove("/tmp/temp_read.txt");
	close(temp_dir_fd);
}

void run_fstat64(struct timespec* start_data,struct timespec* stop_data,struct timespec* empty_start,struct timespec* empty_stop){
	int fd = open("/tmp/text.txt",O_CREAT|O_RDONLY);
	struct stat64 sb;

	clock_gettime(CLOCK_MONOTONIC,start_data);
	fstat64(fd,&sb);
	clock_gettime(CLOCK_MONOTONIC,stop_data);

	clock_gettime(CLOCK_MONOTONIC,empty_start);
	clock_gettime(CLOCK_MONOTONIC,empty_stop);
	close(fd);
	remove("/tmp/text.txt");
}

void run_llseek(struct timespec* start_data,struct timespec* stop_data,struct timespec* empty_start,struct timespec* empty_stop){
	int fd = open("/tmp/text_llseek.txt",O_CREAT|O_RDONLY);

	clock_gettime(CLOCK_MONOTONIC,start_data);
	lseek64(fd,0,SEEK_CUR);
	clock_gettime(CLOCK_MONOTONIC,stop_data);

	clock_gettime(CLOCK_MONOTONIC,empty_start);
	clock_gettime(CLOCK_MONOTONIC,empty_stop);
	close(fd);
	remove("/tmp/text_llseek.txt");
}

void run_pread64(struct timespec* start_data, struct timespec* stop_data, struct timespec* empty_start, struct timespec* empty_stop){
	int fd = open("/tmp/text.txt",O_CREAT|O_RDONLY);
	char buf[5];

	clock_gettime(CLOCK_MONOTONIC,start_data);
	pread64(fd,&buf,1,0);
	clock_gettime(CLOCK_MONOTONIC,stop_data);

	clock_gettime(CLOCK_MONOTONIC,empty_start);
	clock_gettime(CLOCK_MONOTONIC,empty_stop);
	close(fd);
	remove("/tmp/text.txt");
}