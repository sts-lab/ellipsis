IGNORED_ARG = -1

SyscallRelevanceMap = {
    "open"      : [True, False, False, False],
    "close"     : [True, False, False, False],
    "ioctl"     : [True, False, False, False],
    "read"      : [True, False, False, False],
    "pread64"   : [True, False, False, False],
    "write"     : [True, False, False, False],
    "execve"    : [True, True, True, True],
    "openat"    : [True, True, True, True],
    "default"   : [False, False, False, False]
}

SyscallHexMap = {
    "open"      : [True, False, False, False],
    "close"     : [True, False, False, False],
    "ioctl"     : [True, False, False, False],
    "read"      : [True, False, False, False],
    "pread64"   : [True, False, False, False],
    "write"     : [True, False, False, False],
    "execve"    : [False, False, False, False],
    "openat"    : [True, True, True, True],
    "default"   : [False, False, False, False]
}
