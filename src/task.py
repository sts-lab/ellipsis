import time, subprocess, os, pathlib, signal
from auditlib import auditctl
from utils import systemUtils
from utils import ellipsisctl


class TaskRunner:

    def __init__(self, name, config, attack=None):
        self.attack_task       = attack
        self.buffersize        = int(config[name].get('bufferSize', config['DEFAULT']['bufferSize']))
        self.captureConsole    = bool(config[name].get('captureConsole', config['DEFAULT']['captureConsole']))
        self.cmdline           = config[name]['cmdline'].split()
        self.executable        = str(config[name]['executable'])
        self.attack_executable = None
        self.name              = name
        self.syscallsAudit     = str(config[name].get('syscallsToAudit', config['DEFAULT']['syscallsToAudit']))
        self.syscallsBoundary  = str(config[name].get('boundarySystemCalls', config['DEFAULT']['boundarySystemCalls']))
        self.tc_timeout        = int(config[name].get('tc_timeout', config['DEFAULT']['tc_timeout']))
        self.templateGenerated = os.path.join(str(config['DEFAULT']['templateRootFolder']), name)
        self.templateFile      = config[name].get('templateFile', None)
        self.templateFileBad   = config[name].get('templateFileBad', None)
        self.templateDir       = config[name].get('templateDir', None)
        self.timeout           = int(config[name].get('timeout', config['DEFAULT']['timeout']))

        params = {}
        if name == 'ARDUCOPTER' or name == 'PWM_ATTACK':
            params['iters'] = int(config[name]['iters'])
            params['capture_timing'] = bool(config[name]['captureTiming'])
            params['tc_iters'] = int(config[name]['tc_iters'])
        self.params = params

        if name == 'PWM_ATTACK':
            self.attack_cmdline = config[name]['attackCmdline'].split()
            self.attack_executable = str(config[name]['attackExecutable'])


    def audit_setup(self, verbose=False):
        auditctl.set_buffer_size(self.buffersize, not verbose)
        auditctl.add_syscall_rule(self.syscallsAudit, self.executable)
        if self.attack_executable:
            auditctl.add_syscall_rule(self.syscallsAudit, self.attack_executable)


    def finish(self, check_running=False):
        if check_running:
            assert self.process.poll() == None, "Terminated before task ended"
        self.process.kill()
        time.sleep(1)
        while True:
            pid = systemUtils.get_pid(self.executable)
            if pid == None:
                return
            os.kill(pid, signal.SIGKILL)

            if self.attack_executable:
                pid = systemUtils.get_pid(self.attack_executable)
                if pid == None:
                    return
                os.kill(pid, signal.SIGKILL)


    def load_templates(self, reduce=True):
        if not reduce:
            if self.templateFileBad:
                ellipsisctl.load_template(os.path.abspath(self.templateFileBad))
                return
            assert False, "Need a bad template file to force mismatches"
        elif self.templateFile:
            ellipsisctl.load_template(os.path.abspath(self.templateFile))
        elif self.templateDir:
            ellipsisctl.load_template_dir(os.path.abspath(self.templateDir))
        else:
            ellipsisctl.load_template_dir(os.path.abspath(os.path.join(self.templateDir, self.name)))


    def __run_arducopter(self, task_params, tc=False):
        my_env = os.environ.copy()
        my_env['ARDU_TIME'] = ('1' if self.params['capture_timing'] else '0' )
        my_env['ARDU_ITER'] = str(self.params['tc_iters']) if tc else str(self.params['iters'])
        print(self.cmdline)
        self.process = subprocess.run(self.cmdline, universal_newlines=True, capture_output=self.captureConsole, env=my_env)
        #TODO: We can figure out what to do with program output here


    def __run_pwm_attack(self, task_params):
        my_env = os.environ.copy()
        my_env['ARDU_TIME'] = ('1' if self.params['capture_timing'] else '0' )
        my_env['ARDU_ITER'] = str(self.params['iters'])
        copter = subprocess.Popen(self.cmdline, env=my_env)
        print("ArduCopter launched")
        try:
            attack = subprocess.run(self.attack_cmdline, universal_newlines=True, capture_output=self.captureConsole, timeout=self.timeout)
        except subprocess.TimeoutExpired:
            print("PWM attack terminated")
        finally:
            "Wait to cleanup copter"
            copter.wait()


    def __run_cyclictest(self, task_params, tc=False):
        assert os.geteuid() == 0, "root priviledges required to run cyclictest"
        self.cmdline.append('--duration')
        self.cmdline.append(str(self.timeout * 1))
        print(self.cmdline)
        try:
            self.process = subprocess.run(self.cmdline, universal_newlines=True, capture_output=self.captureConsole, timeout=(self.timeout + 1))
        except subprocess.TimeoutExpired:
            print("Process exited as timeout {} expired".format(self.timeout))


    def __run_stress(self, task_params, tc=False, background=True):
        print(self.cmdline)
        if background:
            self.process = subprocess.Popen(self.cmdline)
        else:
            try:
                self.process = subprocess.run(self.cmdline,universal_newlines=True,capture_output=False,timeout=self.timeout)
            except subprocess.TimeoutExpired:
                print("Process exited as timeout {} expired".format(self.timeout))
        return self.process


    def __run_till_timeout(self, task_params, tc=False, background=False):
        timeout = self.tc_timeout if tc else self.timeout
        try:
            print(self.cmdline)
            if background:
                self.process = subprocess.Popen(self.cmdline)
            else:
                self.process = subprocess.run(self.cmdline,universal_newlines=True,capture_output=False,timeout=timeout)
        except subprocess.TimeoutExpired:
            print("Process exited as timeout {} expired".format(timeout))


    def run(self, task_params=None, tc=False, background=False):
        if task_params == None:
            task_params = self.params
        print("\nTask", self.name, "Starting Now")
        if self.name == "ARDUCOPTER":
            self.__run_arducopter(task_params, tc)
        if self.name == "PWM_ATTACK":
            self.__run_pwm_attack(task_params)
        elif self.name == "CYCLICTEST":
            self.__run_cyclictest(task_params, tc)
        elif self.name == "STRESS":
            self.__run_stress(task_params, tc, background)
        else:
            self.__run_till_timeout(task_params, tc, background)
        print("Task Run Done\n")

