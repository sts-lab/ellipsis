#!/usr/bin/python3

"""
template-gen.py generates a template representation given an audit log containing only syscall lines
"""

from asyncio.constants import LOG_THRESHOLD_FOR_CONNLOST_WRITES
from distutils.cmd import Command
import re, os, subprocess, time, sys, argparse, configparser, pathlib, shutil
from task import TaskRunner
from decimal import *
from collections import Counter, defaultdict
from auditlib.audit_parser import *
import config
from auditlib import auditctl
from utils import ellipsisctl, template
from pprint import pprint


def find_splits(sequence, boundary_system_call_nums):
    squashed_output = []
    split_indexes = sorted([i for i, x in enumerate(sequence) if x.syscall in boundary_system_call_nums])
    prev_elem = -1
    for elem in split_indexes:
        # All splits must have at least 2 non boundary syscall
        if elem > prev_elem + 1 and prev_elem >= 0:
            squashed_output.append((prev_elem,elem))
        prev_elem = elem
    return squashed_output


def get_templates(args, indexes, sequence, command):
    tplts = []
    for start,end in indexes:
        if end - start < args.min_template_len + 1:
            continue
        seq = sequence[start + 1: end]
        tplt = template.Template(seq, command, len(tplts), args.temporal_policy)

        found = False
        for t in tplts:
            if t == tplt:
                t.inc_count()
                found = True
                break
        if not found:
            tplts.append(tplt)
    return tplts


def parse_audit_file(args, filename, boundary_system_call_nums, task_template_map, logs_lost, temporal_profiling):
    if args.verbose: print("INFO: Creating template from file:", filename)
    thread_id_map = {}
    with open(filename,"r") as f:
        for line in f.readlines():
            # Filters out non syscall lines, including template match lines
            if 'type=SYSCALL' not in line:
                continue

            # Temporal constraints, but only if there is no log loss
            if 'template=' in line:
                command    = capture_template_name(line).split(".")[0]
                tpl_idx    = capture_template_name(line).split(".")[1]
                start_time = capture_start_time(line)
                end_time   = capture_end_time(line)
                for tpl in task_template_map[command]:
                    if tpl.match_name(command, tpl_idx):
                        tpl.inc_count()
                        if not logs_lost and temporal_profiling:
                            tpl.add_intra_task_duration(end_time - start_time)
                            tpl.add_inter_task_time(start_time)
                        break

            # Collect syscalls to construct templates later
            else:
                syscall = template.Syscall(line)
                try:
                    thread_id_map[syscall.command].append(syscall)
                except KeyError:
                    thread_id_map[syscall.command] = [syscall]

    # Convert syscall lists to templates
    tplts = {}
    for k in thread_id_map.keys():
        sequence = thread_id_map[k]
        sequence = sorted(sequence, key=lambda x: x.timestamp)
        indexes  = find_splits(sequence, boundary_system_call_nums)
        tplts[k] = get_templates(args, indexes, sequence, k)
    task_template_map = merge_templates(task_template_map, tplts)
    return task_template_map

def templates_in_map(m):
    if m == None: return 0
    size = 0
    for k in m.keys():
        size += len(m[k])
    return size


def merge_templates(accum, new):
    if accum == None: return new
    for k in new.keys():
        if not k in accum.keys():
            accum[k] = new[k]
            continue
        for t_new in new[k]:
            if any(t_new == t for t in accum[k]):
                pass
            else:
            # if not t_new in accum[k]:
                accum[k].append(t_new)
                break
    return accum


def main(args, cfg):

    suppress_output = not args.verbose
    task_name = args.taskset
    cmdline = config.get_cmdline(task_name, cfg)
    task_runner = TaskRunner(task_name, cmdline)
    task_params = config.get_task_params(task_name, cfg)
    boundary_system_call_nums = auditctl.get_boundary_syscall_numbers(config.get_boundary_syscalls(task_name, cfg).split(','))
    template_folder = config.get_template_folder(task_name, cfg)
    if os.path.exists(template_folder): shutil.rmtree(template_folder)
    print(task_name, task_params)

    executable = cmdline[0]
    auditctl.set_buffer_size(config.get_buffer_size(task_name, cfg), suppress_output)
    auditctl.clear_syscall_rules(suppress_output)
    auditctl.add_syscall_rule(config.get_syscalls_to_audit(task_name,cfg) + "," + config.get_boundary_syscalls(task_name,cfg),executable, suppress_output)
    task_template_map = None
    template_count_prev = 0
    iters_idx = 0
    iters_idx_templates_done = -1

    while (1):
        # Collect audit logs, process them
        auditctl.clear_audit_logs()
        auditctl.clear_stats(suppress_output)
        ellipsisctl.load_template_dir(template_folder, suppress_output)
        if os.path.exists(template_folder): shutil.rmtree(template_folder)
        auditctl.enable_audit(suppress_output)
        ellipsisctl.toggle_ellipsis(1, suppress_output)

        task_runner.run(task_params)

        auditctl.wait_till_backlog_clear()
        ellipsisctl.toggle_ellipsis(0, suppress_output)
        auditctl.disable_audit(suppress_output)
        logs_lost = auditctl.get_lost_logs()
        ellipsisctl.clear_templates(suppress_output)

        log_file = "/tmp/audit.log"
        auditctl.save_audit_logs(log_file)
        task_template_map = parse_audit_file(args, log_file, boundary_system_call_nums, task_template_map, logs_lost != 0, iters_idx_templates_done > 0)

        for task in task_template_map.keys():
            summary = ""
            for t in task_template_map[task]:
                t.to_file(template_folder)
                summary += t.summary()
            summary_file = pathlib.Path(template_folder).joinpath(task).joinpath("sequence_frequency_summary.txt")
            with summary_file.open(mode="w", encoding="utf-8") as f:
                f.write(summary)
            os.chmod(summary_file, 0o777)


        iters_idx += 1
        print("INFO: Completed Template Generation Iteration =", iters_idx)
        print("INFO: Audit logs lost count =", logs_lost)
        print("INFO: Templates count =", templates_in_map(task_template_map))

        # Loop continuation logic follows
        if iters_idx_templates_done == -1: # Template creation phase
            if iters_idx >= args.iterations_templates and templates_in_map(task_template_map) == template_count_prev:
                iters_idx_templates_done = iters_idx
                if  logs_lost == 0:
                    print("INFO: Template Creation Phase 1 complete. Continuing to temporal profiling.")
                else:
                    print("WARN: Audit logs lost but no new templates, continuing to temporal profiling.")
        elif iters_idx_templates_done > 0:
            if args.skip_temporal_profiling: break
            if iters_idx - iters_idx_templates_done >= args.iterations_temporal: break
        template_count_prev = templates_in_map(task_template_map)

    if args.verbose: print(task_template_map)
    print("INFO: Templates available at", template_folder)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Template Generation Script')
    parser.add_argument('taskset',help='Name of taskset to be used for template generation')
    parser.add_argument('--iterations_templates', '-i1', default=1, type=int, help='Min. number of iterations to run to create templates.')
    parser.add_argument('--iterations_temporal', '-i2', default=1, type=int, help='Min. number of iterations to run for temporal constraints.')
    parser.add_argument('--min_template_len', default=1, help='Minimum number of syscalls in templates, default 1 can still be used by Ellipsis-HP.')
    parser.add_argument('--skip_temporal_profiling', action='store_true', help='')
    parser.add_argument('--stop_when_no_loss', action='store_true', help='Stop when no log loss detected, ignoring iterations argument.')
    parser.add_argument('--temporal_policy', default=-1, type=int, help='avg + n*sigma, where arg value is n. -1 to choose max.')
    parser.add_argument('--verbose', action='store_true', help='Verbose logging.')
    args = parser.parse_args()

    cfg = configparser.ConfigParser()
    cfg.read('ellipsis.cfg')

    main(args, cfg)
