import argparse
import os
import subprocess
import time

from auditlib import auditctl
from utils import ellipsisctl
from utils import systemUtils
from utils import template

SYSCALL_ID_BAD = 24 # getuid
SYSCALL_MAP = {
    'getpid': '0',
    'mmap2' : '1',
    'write' : '2',
    'read' : '3',
    'close' : '4',
    'openat' : '5',
    'connect' : '6',
    'mprotect' : '7',
    'fstat64' : '8',
    'llseek' : '9'
}
SYSCALL_ID_MAP = {
    'getpid': '20',
    'mmap2' : '192',
    'write' : '4',
    'read' : '3',
    'close' : '6',
    'openat' : '322',
    'connect' : '283',
    'mprotect' : '125',
    'fstat64' : '197',
    'llseek' : '140'
}

def get_args():
    parser = argparse.ArgumentParser(
        description='Run a microbenchmark all cases')
    parser.add_argument('-a', '--audit', type=str, default="Unaudited",
                        help='auditing state in [Unaudited, LinuxAudit, Ellipsis, Ellipsis2, Ellipsis-W]')
    parser.add_argument('-s', '--syscall', type=str, default="getpid",
                        help='Sysall to run, "all" to run everything supported')
    parser.add_argument('-i', '--iterations', type=str,
                        default='1000', help='Number of iterations')
    parser.add_argument('-t', '--num-threads', type=str,
                        default='1', help='Number of parallel threads to run')
    parser.add_argument('-nrt', '--not-rt-priority',
                        action='store_true', help='do not run with rt priority')
    parser.add_argument('-o', '--output-file-prefix',
                        type=str, default="", help='output prefix')

    args = parser.parse_args()

    if args.not_rt_priority == False:
        args.rt_priority = True  # Default
    else:
        args.rt_priority = False

    return args

def cleanup_all(verbose=False):
    ellipsisctl.toggle_ellipsis2(0, not verbose)
    ellipsisctl.toggle_ellipsis(0, not verbose)
    ellipsisctl.clear_templates(not verbose)
    auditctl.disable_audit(not verbose)
    auditctl.wait_till_backlog_clear()
    auditctl.clear_audit_logs()
    auditctl.clear_syscall_rules()
    auditctl.clear_stats(not verbose)
    time.sleep(1)
    if verbose:
        print("Cleanup Done, actual test setup starting")


def read_audit_times_from_file(filename):
    timestamps = []
    with open(filename, 'r') as audited_file:
        lines = audited_file.readlines()
        for line in lines:
            if len(line.split()) >= 8:
                timing = int(line.split()[8])/1000.0
                timestamps.append(timing)
    # Discard the first 10 measurements, as they seem to be outliers due to cache effects or other issues starting up
    return timestamps[10:]


def run_microbenchmark(syscall, iterations, num_threads, output_file_prefix, rt_priority, audit):

    print("Test parameters :\n syscall : {} \n iterations : {} \n num_threads : {} \n output_file_prefix : {} \n rt_priority : {} \n audit : {}".format(
        syscall, iterations, num_threads, output_file_prefix, rt_priority, audit))

    executable = os.path.join(os.path.dirname(os.path.realpath(__file__)), "bin/microbench")
    outputfile = output_file_prefix + "-" + audit + '.log'
    
    systemUtils.setup_perf_mode()
    systemUtils.check_isolcpu([3])
    cleanup_all()

    if audit in ["LinuxAudit", "Ellipsis", "Ellipsis2", "Ellipsis-W"]:
        auditctl.add_syscall_rule(syscall, executable)
        auditctl.set_buffer_size(10000)

    if audit in ["Ellipsis", "Ellipsis2"]:
        filename = template.create_template_file(str(1), [SYSCALL_ID_MAP[syscall]])
        ellipsisctl.load_template(filename, True)
        os.remove(filename) 

    if audit in ["Ellipsis-W"]:
        filename = template.create_template_file(str(2), [SYSCALL_ID_MAP[syscall], SYSCALL_ID_BAD])
        ellipsisctl.load_template(filename, True)
        os.remove(filename) 

    if audit in ["LinuxAudit", "Ellipsis", "Ellipsis2", "Ellipsis-W"]:
        auditctl.enable_audit()

    if audit in ["Ellipsis", "Ellipsis2", "Ellipsis-W"]:
        ellipsisctl.toggle_ellipsis(1)

    if audit in ["Ellipsis2", "Ellipsis-W"]:
        ellipsisctl.toggle_ellipsis2(1)

    with open(outputfile, 'w') as outfile:
        if rt_priority:
            cmdline = ['chrt', '--fifo', "99", executable, iterations, num_threads, SYSCALL_MAP[syscall]]
        else:
            cmdline = [executable, iterations, num_threads, SYSCALL_MAP[syscall]]
        print(cmdline)
        subprocess.run(cmdline, stdout=outfile)

    auditctl.wait_till_backlog_clear()
    auditctl.save_audit_logs(outputfile.replace(".log", ".audit"))
    return


def main(args):
    if args.syscall == 'all':
        for scall in SYSCALL_MAP.keys():
            run_microbenchmark(
                scall, args.iterations, args.num_threads, scall, args.rt_priority, args.audit)
    else:
        run_microbenchmark(args.syscall, args.iterations, args.num_threads,
                           args.output_file_prefix, args.rt_priority, args.audit)
    cleanup_all()

if __name__ == "__main__":
    args = get_args()
    main(args)
