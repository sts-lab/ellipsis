#!/usr/bin/python3

import atexit, json,  os, random, subprocess, sys, time
from tqdm import tqdm
from auditlib import auditctl
from utils import ellipsisctl, template, systemUtils

TASKSETS = 1000
SYSCALL_PER_TASK_MAX = 370
HYPERPERIOD = 10
NUM_TASKS = 5

TASK_RUNNER = "/home/pi/rt-audit-internal/src/bin/sched_eval"
LINES_PER_SYSCALL = 3

CASE_LIST = ["Baseline", "Audit", "Ellipsis-B", "Ellipsis-W"]

# [getpid, getppid, getgid32, getuid32, getpgrp]
syscall_ids = [20, 64, 200, 199, 65]
syscall_max_lats = [0.63/100,0.63/100,0.63/100,0.815/100,0.704/100]  #max computed from microbenchmarks
SYSCALL_ID_BAD = 24 # getuid

# In this script we set audit rule only once, so don't clear rules here
def cleanup_all():
    ellipsisctl.toggle_ellipsis2(0, True)
    ellipsisctl.toggle_ellipsis(0, True)
    ellipsisctl.clear_templates(True)
    auditctl.disable_audit(True)
    auditctl.wait_till_backlog_clear()
    auditctl.clear_audit_logs()
    auditctl.clear_stats(True)


def UUniFast(num_tasks, taskset_utilization):
    task_utils = []
    for index in range(1, num_tasks):
        next_util = taskset_utilization * (random.uniform(0,1)**(1.0/(num_tasks-index)))
        task_utils.append(taskset_utilization - next_util)
        taskset_utilization = next_util
    task_utils.append(taskset_utilization)
    return task_utils


def get_task_list(task_utilizations, taskset_util, syscall_ratio):
    periods = [i for i in range(1, HYPERPERIOD + 1) if HYPERPERIOD % i == 0]
    syscall_hyperperiod = 0
    tasks = []

    for idx, util in enumerate(task_utilizations):
        period = random.choice(periods)
        wcet = period * util
        syscall_num = int((wcet * syscall_ratio) / syscall_max_lats[idx])
        non_syscall_time = wcet * (1.0 - syscall_ratio)
        if syscall_num == 0:
            # too small unit to fit syscalls, we need to ignore this task from taskset utilization,
            # so we remove this task completely, in the task_runner.c
            taskset_util -= util
        
        iters_task = HYPERPERIOD // period
        tasks.append([period, syscall_num, iters_task, non_syscall_time])
        syscall_hyperperiod += ((syscall_num * iters_task) + 1)
        if syscall_num > 0:
            syscall_hyperperiod += 1 #add one for the warmup

    tasks = sorted(tasks, key = lambda x: x[0])
    taskset_util = max(0, taskset_util)
    
    return tasks, taskset_util, syscall_hyperperiod


def create_load_template(syscall_seq, reduce = True):
    if len(syscall_seq) == 0:
        return
    print("Length of syscall sequence", len(syscall_seq))

    if not reduce:
        syscall_seq[-1] = SYSCALL_ID_BAD
    filename = template.create_template_file(str(len(syscall_seq)), syscall_seq)
    ellipsisctl.load_template(filename, True)
    os.remove(filename)    


def run_sched_test(syscall_ratio, taskset_count = TASKSETS):
    results = dict()
    for i in tqdm(range(taskset_count)):
        taskset_util = random.random()
        task_utilizations = UUniFast(NUM_TASKS, taskset_util)

        tasks, taskset_util, syscall_hyperperiod = get_task_list(task_utilizations, taskset_util, syscall_ratio)

        auditctl.set_buffer_size(syscall_hyperperiod * LINES_PER_SYSCALL, True)
        
        if any([True for task in tasks if task[1] >= SYSCALL_PER_TASK_MAX]):
            print("Dropping taskset due to syscall length", tasks)
            continue

        if taskset_util not in results.keys():
            results[taskset_util] = dict()
        if syscall_ratio not in results[taskset_util].keys():
            results[taskset_util][syscall_ratio] = dict()

        for case in CASE_LIST:
            cleanup_all()

            results[taskset_util][syscall_ratio][case] = []
            audit_enabled = (case in ["Audit", "Ellipsis-B", "Ellipsis-W"])
            ellipsis_enabled = (case in ["Ellipsis-B", "Ellipsis-W"])
            ellipsis2_enabled = (case in ["Ellipsis-B", "Ellipsis-W"])

            if audit_enabled:
                auditctl.enable_audit(True)
            if ellipsis_enabled:
                for index in range(NUM_TASKS):
                    syscall_seq = [syscall_ids[index]] * tasks[index][1]
                    if case == "Ellipsis-B":
                        create_load_template(syscall_seq, reduce = True)
                    elif case == "Ellipsis-W":
                        create_load_template(syscall_seq, reduce = False)
                ellipsisctl.toggle_ellipsis(1, True)
                if ellipsis2_enabled:
                    ellipsisctl.toggle_ellipsis2(1,True)

            process = subprocess.run([TASK_RUNNER,
                str(tasks[0][1]),   str(tasks[1][1]),   str(tasks[2][1]),   str(tasks[3][1]),   str(tasks[4][1]),
                str(tasks[0][2]*2), str(tasks[1][2]*2), str(tasks[2][2]*2), str(tasks[3][2]*2), str(tasks[4][2]*2),
                str(tasks[0][0]),   str(tasks[1][0]),   str(tasks[2][0]),   str(tasks[3][0]),   str(tasks[4][0]),
                str(tasks[0][3]),   str(tasks[1][3]),   str(tasks[2][3]),   str(tasks[3][3]),   str(tasks[4][3]), str(i)],
                universal_newlines=True, capture_output=False)

            logs_lost = auditctl.get_lost_logs()
            if process.returncode == 255: # this means deadline miss
                success_deadline = 0
            elif process.returncode == 1: # this means normal exec
                success_deadline = 1
            else: # default catch errors
                print("Unexpected return code {}".format(process.returncode))

            if logs_lost == 0:
                success_log_loss = 1
            else:
                success_log_loss = 0

            results[taskset_util][syscall_ratio][case].append([success_deadline, success_log_loss])

    with open("schedulability/res_sched.json", 'w') as res_fd:
        json.dump(results, res_fd)
    print(results)


def main():
    systemUtils.setup_perf_mode()
    systemUtils.check_isolcpu([3])
    systemUtils.pin_audit_daemons_to_cpu([3])

    cleanup_all()
    auditctl.clear_syscall_rules()
    auditctl.add_syscall_rule('getpid,getppid,getgid32,getuid32,getpgrp,getuid' , TASK_RUNNER)

    run_sched_test(0.05)

    cleanup_all()
    auditctl.clear_syscall_rules()


if __name__ == "__main__":
    main()