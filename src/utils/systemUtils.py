import os
import subprocess
import time


def check_cpu_present(cpus):
    cpu_count = os.cpu_count()
    for cpu in cpus:
        assert cpu < cpu_count, "[ERROR] Argument cpu exceed physically present cpus."


def setup_perf_mode(cpus = []):
    cpus = [int(x) for x in cpus]
    check_cpu_present(cpus)
    cpu_count = os.cpu_count()
    if len(cpus) == 0:
        cpus = list(range(cpu_count))
    else:
        cpus = [int(x) for x in cpus]
    for i in cpus:
        with(open('/sys/devices/system/cpu/cpu{}/cpufreq/scaling_governor'.format(str(i)),'w')) as f:
            f.write("performance")
    time.sleep(1)


def check_isolcpu(cpus = []):
    cpus = [int(x) for x in cpus]
    check_cpu_present(cpus)

    isolated_cpus = []
    with open('/proc/cmdline') as f:
        cmd_line = f.read()
    cmd_list = cmd_line.split()
    for cmd in cmd_list:
        if "isolcpus" in cmd:
            isolated_cpus = [int(x) for x in cmd.split("=")[1].split(",")]
            break

    if len(isolated_cpus) == 0:
        print("[ERROR] isolcpus argument not found.")
        return
    
    for cpu in cpus:
        if not cpu in isolated_cpus:
            print("[ERROR] CPU {} not found in isolcpus.".format(str(cpu)))

    return isolated_cpus


def get_pid(name):
    try:
        pid = int(subprocess.check_output(["pidof", "-s", name]))
        return pid
    except:
        return None


def pin_audit_daemons_to_cpu(cpus):
    cpus = [int(x) for x in cpus]
    check_cpu_present(cpus)
    os.sched_setaffinity(get_pid("auditd"), cpus)
    os.sched_setaffinity(get_pid("kauditd"), cpus)
    print("[INFO] auditd and kauditd pinned to cpus", cpus)
