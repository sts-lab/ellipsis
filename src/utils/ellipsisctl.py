import os
import subprocess
import time

CUSTOM_AUDITCTL_PATH = "/home/pi/audit-userspace/src/auditctl"

def toggle_ellipsis(enable, suppress_output=False):
    subprocess.run([CUSTOM_AUDITCTL_PATH, '-T', str(enable)], universal_newlines=True, capture_output=suppress_output)
    if not suppress_output:
        print("\n")
    time.sleep(1)

def toggle_ellipsis2(enable, suppress_output=False):
    subprocess.run([CUSTOM_AUDITCTL_PATH, '-M', str(enable)], universal_newlines=True, capture_output=suppress_output)
    if not suppress_output:
        print("\n")
    time.sleep(1)

def load_template(template_file, suppress_output=False):
    subprocess.run([CUSTOM_AUDITCTL_PATH, '-z', 'file=' + template_file], universal_newlines=True, capture_output=suppress_output)
    if not suppress_output:
        print("\n")
    time.sleep(1)

def load_template_dir(dir, suppress_output):
    if not os.path.exists(dir):
        return
    for (dirpath, _, filenames) in os.walk(dir):
        for filename in filenames:
            path = os.path.join(dirpath, filename)
            if os.path.isfile(path) and ".template" in path:
                load_template(path, suppress_output)

def clear_templates(suppress_output=False):
    subprocess.run([CUSTOM_AUDITCTL_PATH, '-X'], universal_newlines=True, capture_output=suppress_output)
    if not suppress_output:
        print("\n")
    time.sleep(1)

def get_status():
    subprocess.run([CUSTOM_AUDITCTL_PATH, '-s'], universal_newlines=True)
    print("\n")