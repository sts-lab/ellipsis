
import math
import os
import pathlib

from statistics import mean, stdev

from auditlib import audit_parser
from SyscallRelevance import SyscallRelevanceMap, SyscallHexMap, IGNORED_ARG


def _convert_args(hex_args):
    if len(hex_args)%2 == 1:
        hex_args = '0' + hex_args
    return int.from_bytes(bytes.fromhex(hex_args),byteorder='big',signed=True)


class Syscall:
    # Generic contructor that is not expected to be used
    def __init__(self, syscall, thread_id, command, args, timestamp):
        self.syscall    = syscall
        self.sc_name    = audit_parser.syscall_num_to_name(syscall)
        self.thread_id  = thread_id
        self.command    = command
        self.timestamp  = timestamp
        self.init_helper(args)

    # This constructor will be used while parsing audit logs
    def __init__(self, line):
        self.syscall    = audit_parser.capture_syscall(line)
        self.sc_name    = audit_parser.syscall_num_to_name(self.syscall)
        self.thread_id  = audit_parser.capture_thread_id(line)
        self.command    = audit_parser.capture_comm(line)
        self.timestamp  = audit_parser.capture_time(line)
        # self.line       = line
        self.init_helper(audit_parser.capture_args(line))

    def init_helper(self, args):
        args_relevance  = SyscallRelevanceMap.get(self.sc_name, SyscallRelevanceMap["default"])
        args_hex        = SyscallHexMap.get(self.sc_name, SyscallHexMap["default"])
        self.args       = []
        self.str        = str(self.syscall)
        for i,a in enumerate(args):
            if args_relevance[i]:
                self.args.append(args[i])
                if args_hex[i]: self.str += (":" + str(_convert_args(args[i])))
                else: self.str += (":" + str(args[i]))
            else:
                self.args.append(IGNORED_ARG)
                self.str += (":" + str(IGNORED_ARG))

    def __eq__(self, other):
        if not isinstance(other, Syscall): return False
        if self.syscall != other.syscall: return False
        if self.command != other.command: return False
        for i,_ in enumerate(self.args):
            if self.args[i] != other.args[i]: return False
        return True

    def __str__(self):
        return self.str

    def __repr__(self):
        return self.str


class Template:
    def __init__(self, seq, command, index, temporal_policy, count = 1):
        self.len              = len(seq)
        self.syscalls         = seq
        self.count            = count
        self.command          = command
        self.index            = index
        self.intra_task_times = []
        self.inter_task_times = []
        self.inter_task_prev  = -1
        self.temporal_policy  = temporal_policy

    def __eq__(self, other):
        if not isinstance(other, Template): return False
        if not self.len == other.len: return False
        if not self.command == other.command: return False
        for i,_ in enumerate(self.syscalls):
            if self.syscalls[i] != other.syscalls[i]:
                print("syscall diff", self.syscalls[i], other.syscalls[i])
                return False
        return True

    def template_as_string(self):
        ret  = str(self.command) + "." + str(self.index) + "\n"
        ret += str(self.len) + "\n"
        ret += str(self.get_intra_task_constraint()) + "\n"
        ret += str(self.get_inter_task_constraint()) + "\n"
        for entry in self.syscalls: ret += str(entry) + "\n"
        return ret

    def __str__(self):
        return self.template_as_string()

    def __repr__(self):
        return self.template_as_string()

    def to_file(self, template_folder):
        task_folder_path = pathlib.Path(template_folder).joinpath(self.command)
        task_folder_path.mkdir(parents=True, exist_ok=True)
        os.chmod(task_folder_path, 0o777)

        template_file = pathlib.Path(task_folder_path).joinpath(str(self.index) + '.template')
        with template_file.open(mode="w", encoding="utf-8") as f:
            f.write(str(self))
        os.chmod(template_file, 0o777)

    def match_name(self, command, index):
        return str(self.command) == str(command) and int(self.index) == int(index)

    def add_intra_task_duration(self, duration):
        self.intra_task_times.append(duration)

    def add_inter_task_time(self, timestamp):
        if self.inter_task_prev == -1:
            self.inter_task_prev = timestamp
        else:
            self.inter_task_times.append(timestamp - self.inter_task_prev)

    def get_intra_task_constraint(self):
        if len(self.intra_task_times) == 0: return 0
        if len(self.intra_task_times) == 1: return self.intra_task_times[0]
        if self.temporal_policy == -1: return max(self.intra_task_times)
        return math.ceil(mean(self.intra_task_times) + self.temporal_policy * stdev(self.intra_task_times))

    def get_inter_task_constraint(self):
        if len(self.inter_task_times) == 0: return 0
        if len(self.inter_task_times) == 1: return self.inter_task_times[0]
        if self.temporal_policy == -1: return max(self.inter_task_times)
        return math.ceil(mean(self.inter_task_times) + self.temporal_policy * stdev(self.inter_task_times))

    def inc_count(self):
        self.count += 1

    def summary(self):
        return str(self.index) + ":" + str(self.count) + '\n'

def create_template_file(task_name,syscall_seq,intra_task_constraint=0,inter_task_constraint=0):
    tpl_filename = task_name + ".template"
    with open(tpl_filename,'w') as f:
        f.write(task_name + "\n")
        f.write(str(len(syscall_seq)) + "\n")
        f.write(str(intra_task_constraint) + "\n")
        f.write(str(inter_task_constraint) + "\n")
        for syscall in syscall_seq:
            f.write(str(syscall) + ":-1:-1:-1:-1:0\n")

    return tpl_filename



def get_log_line(sequence):
    return str(sequence[1])

def get_timestamp(sequence):
    return sequence[0]

def get_sequence_identifier(sequence):
    return ', '.join(map(get_log_line,sequence))

#Compute timestamp deltas between system calls
def update_timing_delta(curr, tstamps):
    new_delta = [j-i for i,j in zip(tstamps[:-1],tstamps[1:])]
    return [max(t1, t2) for (t1,t2) in zip(curr,new_delta)]

def update_total_tpl_time(curr, tstamps):
    return max(curr, tstamps[-1] - tstamps[0])



def compute_max_deltas(deltas):
    length = len(deltas[0])
    result = []
    for i in range(length):
        result.append(max([row[i] for row in deltas]))

    return result

def compute_stats(raw_data):
    print(raw_data)
    for k in raw_data.keys():
        raw_data[k]['delta'] = compute_max_deltas(raw_data[k]['delta'])
        raw_data[k]['total'] = max(raw_data[k]['total'])

def print_templates_to_file(tpl_data, tpl_thread_map):
    for tpl,tid in tpl_thread_map.items():
        template_entries = tpl.split(',')
        with open(tid+".template","w") as f:
            f.write(tid + "\n")
            f.write(str(len(template_entries)) + "\n")
            f.write(str(tpl_data[tpl]['total']) + "\n")
            tpl_data[tpl]['delta'].insert(0,"")
            for entry,delta in zip(template_entries,tpl_data[tpl]['delta']):
                f.write("{}:{}\n".format(entry,delta))
    return

def write_raw_stats(raw_data, tpl_thread_map):
    with open('tpl-total-time.stats',"w") as f:
        f.write("template,\ttotal_time\n")
        for k in raw_data.keys():
            for entry in raw_data[k]['total']:
                f.write("{},\t{}\n".format(tpl_thread_map[k],entry))

    with open('tpl-delta-time.stats',"w") as f:
        f.write('template,\tseq_num,\tdelta_time\n')
        for k in raw_data.keys():
            for deltas in raw_data[k]['delta']:
                for seq, entry in enumerate(deltas):
                    f.write("{},\t{},\t{}\n".format(tpl_thread_map[k],seq,entry))

    with open('tpl-start-time.stats','w') as f:
        f.write('template,\tstart_time\n')
        for k in raw_data.keys():
            for entry in raw_data[k]['tpl_start']:
                for start_time in entry:
                    f.write("{},\t{}\n".format(tpl_thread_map[k],start_time))

    return
