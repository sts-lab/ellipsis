#!/usr/bin/python3

import matplotlib.pyplot as plt
import json, sys

MARKERS = ['o', '.', 'x', '*']
LINESTYLES = ['-', '--', '-.', ':']

line_Unaudited = None
line_x_un = None

def plot_line(ax, results, case, temporal_only=False):
    global line_Unaudited, line_x_un
    lin_y = []
    for j,constraint in enumerate(['ADM', 'ZLL']):
        if temporal_only:
            if constraint == 'ZLL':
                continue
        line_x = []
        line_y = []
        for utilization in sorted(results.keys()):
            total_res = len(results[utilization][case])
            if total_res == 0: continue
            line_x.append(utilization)
            res_ti = 0
            res_ai = 0
            for res in results[utilization][case]:
                res_ti += res[0]
                res_ai += res[1]
            if constraint == 'ADM':
                line_y.append(res_ti/total_res)
            else:
                line_y.append(res_ai/total_res)
        lin_y.append(line_y)
        case_label = case
        if case == "Baseline":
            line_Unaudited = line_y
            line_x_un = line_x
            return
        if case_label == "Ellipsis-W":
            case_label = "Ellipsis NR"
        if case_label == "Ellipsis-B":
            case_label = "Ellipsis B"

        ax.plot(line_x, line_y, label = case_label + " " + constraint, marker=MARKERS[j], linestyle=LINESTYLES[j])
        ax.set_ylim(0, 1.1)
    print (lin_y)
    min_line = []
    for i,r in enumerate(lin_y[0]):
        min_line.append(min(lin_y[0][i], lin_y[1][i]))
    
    ax.fill_between(line_x, 0, min_line, color='green', alpha=0.2)


with open("res_sched.json", 'r') as res_fd:
    results = json.load(res_fd)


res = dict()
for num in results.keys():
    if '0.05' in results[num].keys():
        res[float(num)] = results[num]['0.05']
results = res

binned_results = dict()
for num in results.keys():
    target_bin = round(float(num),1)
    old_value = binned_results.get(target_bin,dict())
    updated_value = dict()
    cases = results[num].keys()
    for case in cases:
        updated_value[case] = old_value.get(case,list()) + results[num][case]
    binned_results[target_bin] = updated_value

fig, ax = plt.subplots(1, len(binned_results[list(binned_results.keys())[0]].keys()) - 1,
                                sharex=True, sharey=True, tight_layout=False, figsize=(13,2))

for i,case in enumerate(binned_results[list(binned_results.keys())[0]].keys()):
    if case == "Baseline":
        plot_line(None, binned_results, case, temporal_only=(case == "Baseline"))
        continue
    plot_line(ax[i-1], binned_results, case, temporal_only=(case == "Baseline"))


print(line_x_un, line_Unaudited)
ax[0].plot(line_x_un, line_Unaudited, label = "Baseline", marker=MARKERS[2], linestyle=LINESTYLES[2], color='black')
ax[1].plot(line_x_un, line_Unaudited, label = "Baseline", marker=MARKERS[2], linestyle=LINESTYLES[2], color='black')
ax[2].plot(line_x_un, line_Unaudited, label = "Baseline", marker=MARKERS[2], linestyle=LINESTYLES[2], color='black')

for i,case in enumerate(binned_results[list(binned_results.keys())[0]].keys()):
    if case == "Baseline": continue
    ax[i - 1].legend(loc='lower center', bbox_to_anchor=(0.5, 1.01), ncol=2)

ax[0].set_ylabel("Acceptance Ratio")
ax[0].set_xlabel('Utilization')
ax[1].set_xlabel('Utilization')
ax[2].set_xlabel('Utilization')

for a in ax.flatten():
    a.yaxis.set_tick_params(labelleft=True)


plt.savefig("sched.pdf",format='pdf',bbox_inches='tight',dpi=500)
