#define _GNU_SOURCE
#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include <sys/types.h>
#include <pthread.h>
#include <stdatomic.h>

#define RET_FAIL -1
#define RET_PASS 1
#define NSEC_PER_SEC (1000 * 1000 * 1000ULL)
#define MS (1 * 1000 * 1000ULL) // 1 ms in nanos
#define US (100 * 1000ULL)      // 100 us in nanos
#define NUM_TASKS 5
#define DUMP_TO_FILES 0

pthread_barrier_t barrier;
pthread_t dead_task;
pthread_t threads[NUM_TASKS];
int task_len[NUM_TASKS], iters[NUM_TASKS], period[NUM_TASKS], result[NUM_TASKS];
float non_syscall_time[NUM_TASKS];
long long int durations[NUM_TASKS][1000];
unsigned long long exec_time[NUM_TASKS][1000];
int task_status[NUM_TASKS][1000];
// unsigned long long start_time;
struct timespec start_time;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

long long timespec_subtract(struct timespec *a, struct timespec *b)
{
    long long ns;
    ns = (b->tv_sec - a->tv_sec) * NSEC_PER_SEC;
    ns += (b->tv_nsec - a->tv_nsec);
    return ns;
}

void timespec_from_nsec(struct timespec *a, u_int64_t b)
{
    a->tv_sec = b / NSEC_PER_SEC;
    a->tv_nsec = b % NSEC_PER_SEC;
}

unsigned long long time_from_timespec(struct timespec *t)
{
    unsigned long long ns;
    ns = t->tv_sec * NSEC_PER_SEC;
    ns += t->tv_nsec;
    return ns;
}

void *run_load(int index)
{
    int i, j, aggr, ret, deadline_fail = 0;
    struct timespec start_data, stop_data, empty_start, empty_stop, sleep_duration, time_remaining, *ptr_actual, *ptr_remain, *tmp;
    unsigned long long duration, time_to_sleep;
    
    struct sched_param param;
    param.sched_priority = 10 - index;
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(3, &cpuset);
    pthread_setschedparam(threads[index], SCHED_FIFO, &param);
    pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);

    // Warmup for caches
    switch (index)
    {
    case 0:
        getpid();
        break;
    case 1:
        getppid();
        break;
    case 2:
        getgid();
        break;
    case 3:
        getuid();
        break;
    case 4:
        getpgrp();
        break;
    default:
        assert(0);
    }

    // We want the last update to start_time as the global start time.
    // The barrier afterwards ensures all threads become ready at the same time.
    pthread_mutex_lock(&lock);
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    pthread_mutex_unlock(&lock);

    pthread_barrier_wait(&barrier);

    for (i = 0; i < iters[index]; i++)
    {
        aggr = 0;

        for (j = 0; j < task_len[index]; j++)
        {
            switch (index)
            {
            case 0:
                aggr += (int)getpid();
                break;
            case 1:
                aggr += (int)getppid();
                break;
            case 2:
                aggr += (int)getgid();
                break;
            case 3:
                aggr += (int)getuid();
                break;
            case 4:
                aggr += (int)getpgrp();
                break;
            default:
                assert(0);
            }
        }

        clock_gettime(CLOCK_MONOTONIC, &start_data);
        clock_gettime(CLOCK_MONOTONIC, &stop_data);
        while (timespec_subtract(&start_data, &stop_data) < (non_syscall_time[index] * US))
        {
            clock_gettime(CLOCK_MONOTONIC, &stop_data);
        }

        unsigned long long max_task_offset = period[index] * US * (i + 1);
        unsigned long long task_offset = timespec_subtract(&start_time, &stop_data);
        durations[index][i] = max_task_offset - task_offset;
        long long time_remaining = max_task_offset - task_offset;

        if (time_remaining < 0)
        {
            result[index] = 1;
            task_status[index][i] = 1;
        }
        else
        {
            time_to_sleep = durations[index][i];
            ptr_actual = &sleep_duration;
            ptr_remain = &time_remaining;
            timespec_from_nsec(&sleep_duration, time_to_sleep);
            while ((ret = nanosleep(ptr_actual, ptr_remain)) && errno == EINTR)
            {
                tmp = ptr_actual;
                ptr_actual = ptr_remain;
                ptr_remain = tmp;
            }
        }
    }
}

int main(int argc, char *argv[])
{
    int i;

    task_len[0] = atoi(argv[1]);
    task_len[1] = atoi(argv[2]);
    task_len[2] = atoi(argv[3]);
    task_len[3] = atoi(argv[4]);
    task_len[4] = atoi(argv[5]);

    iters[0] = atoi(argv[6]);
    iters[1] = atoi(argv[7]);
    iters[2] = atoi(argv[8]);
    iters[3] = atoi(argv[9]);
    iters[4] = atoi(argv[10]);

    // In increasing order of period, from python script
    period[0] = atoi(argv[11]);
    period[1] = atoi(argv[12]);
    period[2] = atoi(argv[13]);
    period[3] = atoi(argv[14]);
    period[4] = atoi(argv[15]);

    non_syscall_time[0] = atof(argv[16]);
    non_syscall_time[1] = atof(argv[17]);
    non_syscall_time[2] = atof(argv[18]);
    non_syscall_time[3] = atof(argv[19]);
    non_syscall_time[4] = atof(argv[20]);

    int task_number = atoi(argv[21]);
    char filename[100], filename1[100];
    sprintf(filename, "task_durations_%04d", task_number);
    sprintf(filename1, "task_status_%04d", task_number);

#if DUMP_TO_FILES
    FILE *f = fopen(filename, "wb");
    FILE *f1 = fopen(filename1, "wb");
    if (f == NULL || f1 == NULL)
    {
        printf("Error opening file %s\n", filename);
        exit(30);
    }
#endif

    int useful_tasks = 0;
    for (i = 0; i < NUM_TASKS; i++)
    {
        if (task_len[i] > 0)
        {
            useful_tasks++;
        }
    }

    pthread_barrier_init(&barrier, NULL, useful_tasks);

    for (i = 0; i < NUM_TASKS; i++)
    {
        if (task_len[i] > 0)
        {
            // only start a thread if there is some syscalls to run
            printf("Starting task %d\n", i);
            pthread_create(&threads[i], NULL, run_load, i);
        }
        else
        {
            threads[i] = -1;
        }
    }

    for (i = 0; i < NUM_TASKS; i++)
    {
        if (threads[i] != -1)
        {
            pthread_join(threads[i], NULL);
        }
    }

#if DUMP_TO_FILES
    for(int i = 0; i < NUM_TASKS; i++) {
        for(int j = 0; j < iters[i]; j++) {
            fprintf(f,"Task: %d Len: %d Iteration: %d Duration %lld\n",i,task_len[i],j,durations[i][j]);
            fprintf(f1,"Task: %d Period: %d Iteration: %d Status: %d\n",i,period[i],j,task_status[i][j]);
        }
    }

    fclose(f);
    fclose(f1);
#endif

    for (i = 0; i < NUM_TASKS; i++)
    {
        if (result[i])
        {
            exit(RET_FAIL);
        }
    }
    return RET_PASS;
}
